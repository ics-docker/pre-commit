FROM registry.esss.lu.se/ics-docker/mambaforge:23.11.0-0
ENV PRE_COMMIT_VERSION 4.0.1

# Install several Python versions
RUN conda create -y -c conda-forge -n python36 python=3.6 \
  && conda create -y -c conda-forge -n python37 python=3.7 \
  && conda create -y -c conda-forge -n python38 python=3.8 \
  && conda create -y -c conda-forge -n python39 python=3.9 \
  && conda create -y -c conda-forge -n python310 python=3.10 \
  && conda create -y -c conda-forge -n python311 python=3.11 pre-commit=${PRE_COMMIT_VERSION} \
  && conda clean -ay

ENV PATH /opt/conda/envs/python311/bin:/opt/conda/envs/python310/bin:/opt/conda/envs/python39/bin:/opt/conda/envs/python38/bin:/opt/conda/envs/python37/bin:/opt/conda/envs/python36/bin:$PATH

COPY pip.conf /etc/pip.conf

# Run pre-commit with a pre-generated .pre-commit-config.yaml
# to have dependencies already installed in the image.
RUN mkdir /tmp/pre-commit
COPY .pre-commit-config-for-build.yaml /tmp/pre-commit/.pre-commit-config.yaml
RUN cd /tmp/pre-commit \
  && git init . \
  && pre-commit run
