pre-commit docker image
=======================

Python docker image with [pre-commit](https://pre-commit.com) included.

This image is used to run all pre-commit hooks in GitLab CI.
Add the following to your `.gitlab-ci.yml`:
```
check:
  tags:
    - docker
  stage: check
  image: registry.esss.lu.se/ics-docker/pre-commit:latest
  script:
    - pre-commit run --all-files
```

Your project should of course include a `.pre-commit-config.yaml` file.


Docker pull command:

```
docker pull registry.esss.lu.se/ics-docker/pre-commit:latest
```
